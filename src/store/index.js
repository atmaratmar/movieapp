import Vue from 'vue'
import Vuex from 'vuex'
import {LoginAPI} from "@/components/Login/LoginAPI"
import {MoviesAPI} from "@/components/Movie/MoviesAPI"
// we can make som opration map
//const tore = require('./vuex/store').default;

Vue.use(Vuex)

export default new Vuex.Store({
    state:{
        username :'',
        password: '',
        profile:{},
        movies:[],
        searchText:'',
        error:''
           },
    mutations:{
        setProfile:(state , payload)=>{
            state.profile=payload;
        },
        setMovies:(state , payload)=>{
            state.movies=payload;
        },
        addFavourite:(state , payload)=>{
            state.profile.favourites=payload;
        },
    
        setUsername:(state , payload)=>{
         state.username=payload;
       },
       setPassword:(state , payload)=>{
        state.password=payload;
      },
      setSearchText:(state,payload)=>{
          state.searchText=payload;
      },
      setError:(state,payload)=>{
          state.error=payload;
      }
    },
      getters:{

      },
      actions:{

          async loginUser({state, commit}){
              try{
                  const loginDetails = JSON.stringify({
                  user:{
                        username : state.username,
                        password : state.password
                       }
                  })
                  const user = await LoginAPI.login(loginDetails)
                  if(user){
                    commit('setProfile' ,user)
                  }else{
                    commit('setError','User not fund')
                  }
                } catch(e){
                  commit('setError', e.message)
                }
            
            },
           async  registerUser({state, commit}){
                try{
                    const registerDetails = JSON.stringify({
                    user:{
                          username : state.username,
                          password : state.password
                         }
                    })
                    const user = await LoginAPI.register(registerDetails)
                    if(user){
                      commit('setProfile' ,user)
                    }else{
                      commit('user not register')
                    }
                  } catch(e){
                    commit('setError', e.message)
                  }
              
          },
          async fetchMovies({ commit }){
            try {
              const movies = await MoviesAPI.addFavourite()
              commit('setMovies',movies)
              
            } catch (error) {
              commit("erro")
            }
          },
          async addFavourite({commit ,state},payload){
            const {profile} = state
            try {
              
              const favourite ={
                movieId : payload,
                userId : profile.id
              }
              const newFavourites = await MoviesAPI.fetchMovies(favourite)
              commit('addFavourite' , newFavourites)
            } catch (error) {
              commit("favourites error")
            }
         
          }
    }
})