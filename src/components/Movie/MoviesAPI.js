export const MoviesAPI = {
    fetchMovies(){
        return fetch("http://noroff-movie-catalogue.herokuapp.com/v1/movies")
        .then(res=> res.json())
        .then(data => data.data)
    },
    addFavourite(favourite){
        const requestOptions={
            method: "POST",
            headers:{"Content-Type":"application/json"},
            //body:favourte
            body:JSON.stringify(favourite)
            // we can either stringfiy or get it from action
        }
        return fetch("http://noroff-movie-catalogue.herokuapp.com/v1/users/favourites" , requestOptions)
        .then(res=> res.json())
        
    }
}