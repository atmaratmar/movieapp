export const LoginAPI ={
    register(registerDetails){
       const requestOptions={
           method: "POST",
           headers:{"Content-Type":"application/json"},
           body:registerDetails
       }
       // I put fetch here but we can use also axis
       return fetch("https://noroff-movie-catalogue.herokuapp.com/v1/users/register", requestOptions)
       .then(res => res.json())
       // we want to return the data back
       .then(data => data.data)
    },
    login(loginDeatils){
        const requestOptions={
            method: "POST",
            headers:{"Content-Type":"application/json"},
            body:loginDeatils
        }
           // we copy and change the end point
       return fetch("https://noroff-movie-catalogue.herokuapp.com/v1/users/login", requestOptions)
       .then(res => res.json())
       // we want to return the data back
       .then(data => data.data)



       // from here we would go stor index.js and write some actions

    }
}